const userPostgree = require('./app/models/User');
const express = require('express');
const routes = express.Router();

routes.post('/order', async (req , res) => {
const { hamburger, drink, follow_up } = req.body;

const createPostgree = await userPostgree.create({ hamburger, drink, follow_up });

return res.json( createPostgree )
});

routes.get('/order', async (req , res) => {  
  const postgree = await userPostgree.findAll();
  
  return res.json( postgree )
  });
  

module.exports = routes;
