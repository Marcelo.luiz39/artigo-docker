const { Sequelize } = require('sequelize')

const Order = require('../app/models/Order')

const databaseConfig = require('../config/database')

const models = [Order]

class Database {
  constructor() {
    this.init()
  }

  init() {
    this.connection = new Sequelize(databaseConfig)

    models.map(model => model.init(this.connection))
  }
}

module.exports = new Database()
