const { Model, Sequelize } = require('sequelize');

class Order extends Model {
  static init(sequelize) {
    super.init(
      {
        hamburger: Sequelize.STRING,
        drink: Sequelize.STRING,
        follow_up: Sequelize.STRING,
      }, {
      sequelize,
    },
    );
  }
}

module.exports = Order;
